// ignore: unused_import
import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../AppTheme.dart';

class Generator {
  static const Color starColor = Color(0xfff9c700);

  static String convertDateTimeToText(DateTime dateTime,
      {bool showSecond = true, bool showDate = true, bool showTime = true}) {
    if (dateTime != null && dateTime.toString() != '-0001-11-30 00:00:00.000') {
      String date = dateTime.day < 10
          ? "0" + dateTime.day.toString()
          : dateTime.day.toString();
      String month = dateTime.month < 10
          ? "0" + dateTime.month.toString()
          : dateTime.month.toString();
      String year = dateTime.year.toString();

      String hour = dateTime.hour < 10
          ? "0" + dateTime.hour.toString()
          : dateTime.hour.toString();
      String minute = dateTime.minute < 10
          ? "0" + dateTime.minute.toString()
          : dateTime.minute.toString();
      String second = "";
      if (showSecond)
        second = dateTime.second < 10
            ? "0" + dateTime.second.toString()
            : dateTime.second.toString();

      if (showDate && !showTime) {
        return date + "-" + month + "-" + year;
      } else if (!showDate && showTime) {
        return hour + ":" + minute + (showSecond ? ":" : "") + second;
      }
      return date +
          "-" +
          month +
          "-" +
          year +
          " " +
          hour +
          ":" +
          minute +
          (showSecond ? ":" : "") +
          second;
    }
    return '-';
  }

  static List<Color> getColorByRating(CustomAppTheme customAppTheme) {
    return [
      customAppTheme.colorError, //For 0 star color
      customAppTheme.colorError, //For 1 star color
      customAppTheme.colorError.withAlpha(200), //For 2 star color
      CustomAppTheme.starColor, //For 3 star color
      customAppTheme.colorSuccess.withAlpha(200), //For 4 star color
      customAppTheme.colorSuccess //For 5 star color
    ];
  }
}
