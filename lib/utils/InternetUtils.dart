// ignore: import_of_legacy_library_into_null_safe
import 'package:connectivity/connectivity.dart'
    show Connectivity, ConnectivityResult;

class InternetUtils {
  static Future<bool> checkConnection() async {
    ConnectivityResult connectivityResult =
        await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }
}
