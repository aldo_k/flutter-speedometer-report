// ignore: unused_import
import 'package:delivery_boy/api/api_util.dart';
// ignore: import_of_legacy_library_into_null_safe
import 'package:url_launcher/url_launcher.dart' show canLaunch, launch;

class UrlUtils {
  static openDocsDownloadPage() {
    openUrl("https://google.com/");
  }

  static openUrl(String url) async {
    if (await canLaunch(url) != null) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  static callFromNumber(String number) {
    openUrl("tel:" + number.toString());
  }

  static openMap(double latitude, double longitude) {
    openUrl("http://maps.google.com/maps?q=$latitude+$longitude");
  }
}
