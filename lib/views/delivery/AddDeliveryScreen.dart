import 'dart:io';

import 'package:delivery_boy/api/api_util.dart';
import 'package:delivery_boy/controllers/AuthController.dart';
import 'package:flutter/services.dart';
import 'package:delivery_boy/controllers/SpeedometerController.dart';
import 'package:delivery_boy/models/DeliveryBoy.dart';
import 'package:delivery_boy/models/MyResponse.dart';
import 'package:delivery_boy/services/AppLocalizations.dart';
import 'package:delivery_boy/utils/SizeConfig.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import '../../AppTheme.dart';
import '../../AppThemeNotifier.dart';
import '../AppScreen.dart';

class AddDeliveryScreen extends StatefulWidget {
  @override
  _AddDeliveryScreenState createState() => _AddDeliveryScreenState();
}

class _AddDeliveryScreenState extends State<AddDeliveryScreen> {
  //ThemeData
  ThemeData themeData;
  CustomAppTheme customAppTheme;

  //Text Field Editing Controller
  TextEditingController licensePlateTFController;
  TextEditingController driverNameTFController;
  TextEditingController digitsKmTFController;

  //Global Key
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      new GlobalKey<ScaffoldMessengerState>();

  //Other Variable
  OutlineInputBorder allTFBorder;

  //Other Variables
  DeliveryBoy deliveryBoy;
  bool isInProgress = false;
  File speedometerFile;
  final picker = ImagePicker();

  _initUserData() async {
    DeliveryBoy user = await AuthController.getUser();
    setState(() {
      deliveryBoy = user;
      licensePlateTFController =
          TextEditingController(text: deliveryBoy.motorcycleLicensePlate);
      driverNameTFController = TextEditingController(text: deliveryBoy.name);
    });
  }

  @override
  void initState() {
    super.initState();
    _initUserData();
    licensePlateTFController = TextEditingController();
    driverNameTFController = TextEditingController();
    digitsKmTFController = TextEditingController();
  }

  @override
  void dispose() {
    licensePlateTFController.dispose();
    driverNameTFController.dispose();
    digitsKmTFController.dispose();
    super.dispose();
  }

  _initUI() {
    allTFBorder = OutlineInputBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(8),
        ),
        borderSide: BorderSide(color: customAppTheme.bgLayer4, width: 1.5));
  }

  _getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        speedometerFile = File(pickedFile.path);
      }
    });
  }

  _handleUpdate() async {
    String digitsKM = digitsKmTFController.text;

    if (digitsKM.isEmpty) {
      showMessage(message: Translator.translate("please_fill_digits_km"));
    } else if (speedometerFile == null) {
      showMessage(message: Translator.translate("please_fill_images_km"));
    } else {
      if (mounted) {
        setState(() {
          isInProgress = true;
        });
      }

      MyResponse myResponse =
          await SpeedometerController.addSpeedometer(digitsKM, speedometerFile);
      if (mounted) {
        setState(() {
          isInProgress = false;
        });
      }

      if (myResponse.success) {
        showMessage(message: Translator.translate("add_successfully"));
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (BuildContext context) => AppScreen(),
          ),
        );
      } else {
        ApiUtil.checkRedirectNavigation(context, myResponse.responseCode);
        showMessage(message: myResponse.errorText);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<AppThemeNotifier>(
      builder: (BuildContext context, AppThemeNotifier value, Widget child) {
        int themeType = value.themeMode();
        themeData = AppTheme.getThemeFromThemeMode(themeType);
        customAppTheme = AppTheme.getCustomAppTheme(themeType);
        _initUI();
        return MaterialApp(
            scaffoldMessengerKey: _scaffoldMessengerKey,
            debugShowCheckedModeBanner: false,
            theme: AppTheme.getThemeFromThemeMode(value.themeMode()),
            home: Scaffold(
                key: _scaffoldKey,
                appBar: AppBar(
                  elevation: 0,
                  leading: IconButton(
                    onPressed: () => Navigator.of(context).pop(),
                    icon: Icon(
                      MdiIcons.chevronLeft,
                      color: themeData.colorScheme.onBackground,
                    ),
                  ),
                  centerTitle: true,
                  title: Text(
                    Translator.translate("add_delivery_log"),
                    style: AppTheme.getTextStyle(themeData.textTheme.headline6,
                        color: themeData.colorScheme.onBackground,
                        fontWeight: 600),
                  ),
                ),
                body: Column(
                  children: [
                    Container(
                      height: MySize.size3,
                      child: isInProgress
                          ? LinearProgressIndicator(
                              minHeight: MySize.size3,
                            )
                          : Container(
                              height: MySize.size3,
                            ),
                    ),
                    Expanded(child: _buildBody())
                  ],
                )));
      },
    );
  }

  _buildBody() {
    if (deliveryBoy == null) {
      return Container();
    } else {
      return ListView(
        children: <Widget>[
          Container(
            padding: Spacing.all(24),
            child: Column(
              children: <Widget>[
                Container(
                  margin: Spacing.top(8),
                  child: TextFormField(
                    enabled: false,
                    style: AppTheme.getTextStyle(
                      themeData.textTheme.bodyText1,
                      letterSpacing: 0.1,
                      color: themeData.colorScheme.onBackground,
                      fontWeight: 500,
                    ),
                    decoration: InputDecoration(
                      hintText:
                          Translator.translate("motorcycle_license_plate"),
                      hintStyle: AppTheme.getTextStyle(
                        themeData.textTheme.subtitle2,
                        letterSpacing: 0.1,
                        color: themeData.colorScheme.onBackground,
                        fontWeight: 500,
                      ),
                      border: allTFBorder,
                      enabledBorder: allTFBorder,
                      focusedBorder: allTFBorder,
                      prefixIcon: Icon(
                        MdiIcons.mopedOutline,
                        size: MySize.size22,
                      ),
                      isDense: true,
                      contentPadding: Spacing.zero,
                    ),
                    keyboardType: TextInputType.text,
                    controller: licensePlateTFController,
                  ),
                ),
                Container(
                  margin: Spacing.top(8),
                  child: TextFormField(
                    enabled: false,
                    style: AppTheme.getTextStyle(
                      themeData.textTheme.bodyText1,
                      letterSpacing: 0.1,
                      color: themeData.colorScheme.onBackground,
                      fontWeight: 500,
                    ),
                    decoration: InputDecoration(
                      hintText: Translator.translate("driver_name"),
                      hintStyle: AppTheme.getTextStyle(
                        themeData.textTheme.subtitle2,
                        letterSpacing: 0.1,
                        color: themeData.colorScheme.onBackground,
                        fontWeight: 500,
                      ),
                      border: allTFBorder,
                      enabledBorder: allTFBorder,
                      focusedBorder: allTFBorder,
                      prefixIcon: Icon(
                        MdiIcons.accountOutline,
                        size: MySize.size22,
                      ),
                      isDense: true,
                      contentPadding: Spacing.zero,
                    ),
                    keyboardType: TextInputType.text,
                    controller: driverNameTFController,
                  ),
                ),
                InkWell(
                  onTap: () {
                    _getImage();
                  },
                  child: Container(
                    margin: Spacing.top(8),
                    padding: Spacing.all(4),
                    decoration: BoxDecoration(
                      color: customAppTheme.bgLayer1,
                      borderRadius: BorderRadius.all(
                        Radius.circular(MySize.size4),
                      ),
                      border: Border.all(
                        color: customAppTheme.bgLayer4,
                        width: 1,
                      ),
                    ),
                    child: SizedBox(
                      width: double.infinity,
                      child: Container(
                        margin: Spacing.top(8),
                        child: Column(
                          children: <Widget>[
                            Padding(
                              padding: EdgeInsets.only(
                                bottom: 4,
                              ),
                              child: ClipRRect(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(
                                    MySize.getScaledSizeWidth(100),
                                  ),
                                ),
                                child: speedometerFile == null
                                    ? Image.asset(
                                        './assets/images/upload-icon.png',
                                        color: themeData.colorScheme.primary,
                                        width: MySize.getScaledSizeWidth(128),
                                        height: MySize.getScaledSizeWidth(128),
                                      )
                                    : Image.file(
                                        speedometerFile,
                                        width: MySize.getScaledSizeWidth(128),
                                        height: MySize.getScaledSizeWidth(128),
                                        fit: BoxFit.cover,
                                      ),
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.only(
                                top: 4,
                              ),
                              child: speedometerFile == null
                                  ? Text(
                                      Translator.translate(
                                          "upload_speedometer"),
                                    )
                                  : Text(
                                      Translator.translate(
                                          "update_speedometer"),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: Spacing.top(8),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.circular(MySize.size8)),
                  ),
                  child: SizedBox(
                      width: double.infinity,
                      child: Text(Translator.translate("6_digits"))),
                ),
                Container(
                  margin: Spacing.top(8),
                  child: TextFormField(
                    style: AppTheme.getTextStyle(
                      themeData.textTheme.bodyText1,
                      letterSpacing: 0.1,
                      color: themeData.colorScheme.onBackground,
                      fontWeight: 500,
                    ),
                    decoration: InputDecoration(
                      hintText: Translator.translate("speedometer"),
                      hintStyle: AppTheme.getTextStyle(
                        themeData.textTheme.subtitle2,
                        letterSpacing: 0.1,
                        color: themeData.colorScheme.onBackground,
                        fontWeight: 500,
                      ),
                      border: allTFBorder,
                      enabledBorder: allTFBorder,
                      focusedBorder: allTFBorder,
                      prefixIcon: Icon(
                        MdiIcons.clockTimeEightOutline,
                        size: MySize.size22,
                      ),
                      isDense: true,
                      contentPadding: Spacing.zero,
                    ),
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      // ignore: deprecated_member_use
                      WhitelistingTextInputFormatter.digitsOnly,
                      LengthLimitingTextInputFormatter(6),
                    ],
                    controller: digitsKmTFController,
                  ),
                ),
                Container(
                  margin: Spacing.top(8),
                  decoration: BoxDecoration(
                    borderRadius:
                        BorderRadius.all(Radius.circular(MySize.size8)),
                    boxShadow: [
                      BoxShadow(
                        color: themeData.colorScheme.primary.withAlpha(20),
                        blurRadius: 3,
                        offset: Offset(0, 1),
                      ),
                    ],
                  ),
                  child: SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      style: ButtonStyle(
                        padding: MaterialStateProperty.all(Spacing.xy(24, 12)),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4),
                          ),
                        ),
                      ),
                      onPressed: () {
                        _handleUpdate();
                      },
                      child: Text(
                        Translator.translate("save").toUpperCase(),
                        style: AppTheme.getTextStyle(
                            themeData.textTheme.caption,
                            fontWeight: 600,
                            color: themeData.colorScheme.onPrimary,
                            letterSpacing: 0.4),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    }
  }

  void showMessage({String message = "Terjadi Kesalahan", Duration duration}) {
    if (duration == null) {
      duration = Duration(seconds: 3);
    }
    _scaffoldMessengerKey.currentState.showSnackBar(
      SnackBar(
        duration: duration,
        content: Text(message,
            style: AppTheme.getTextStyle(themeData.textTheme.subtitle2,
                letterSpacing: 0.4, color: themeData.colorScheme.onPrimary)),
        backgroundColor: themeData.colorScheme.primary,
        behavior: SnackBarBehavior.fixed,
      ),
    );
  }
}
