import 'package:delivery_boy/api/api_util.dart';
import 'package:delivery_boy/models/MyResponse.dart';
import 'package:delivery_boy/models/Speedometer.dart';
import 'package:delivery_boy/controllers/SpeedometerController.dart';
import 'package:delivery_boy/services/AppLocalizations.dart';
// import 'package:delivery_boy/utils/ColorUtils.dart';
import 'package:delivery_boy/utils/Generator.dart';
import 'package:delivery_boy/utils/SizeConfig.dart';
import 'package:delivery_boy/views/LoadingScreens.dart';
import 'package:delivery_boy/views/auth/SettingScreen.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:provider/provider.dart';

import '../AppTheme.dart';
import '../AppThemeNotifier.dart';
import 'delivery/AddDeliveryScreen.dart';
import 'delivery/ClosingDeliveryScreen.dart';

import 'dart:developer';

class AppScreen extends StatefulWidget {
  @override
  _AppScreenState createState() => _AppScreenState();
}

class _AppScreenState extends State<AppScreen> with WidgetsBindingObserver {
  //ThemeData
  ThemeData themeData;
  CustomAppTheme customAppTheme;

  //Global Key
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<ScaffoldMessengerState> _scaffoldMessengerKey =
      new GlobalKey<ScaffoldMessengerState>();
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  //Other Variables
  bool isInProgress = false;
  // ignore: non_constant_identifier_names
  List<Speedometer> speedometers = [];

  initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    _loadSpeedometerData();
  }

  Future<void> _refresh() async {
    if (!isInProgress) _loadSpeedometerData();
  }

  _loadSpeedometerData() async {
    setState(() {
      isInProgress = true;
    });
    MyResponse<List<Speedometer>> myResponse =
        await SpeedometerController.getAllSpeedometer();

    if (myResponse.success) {
      speedometers = myResponse.data;
    } else {
      ApiUtil.checkRedirectNavigation(context, myResponse.responseCode);

      showMessage(message: myResponse.errorText);
    }

    setState(() {
      isInProgress = false;
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      _refresh();
    } else if (state == AppLifecycleState.inactive) {
      log('InActive');
    } else if (state == AppLifecycleState.paused) {
      log('Pause');
    } else if (state == AppLifecycleState.detached) {
      log('Detached');
    }
  }

  @override
  void dispose() {
    super.dispose();
    WidgetsBinding.instance.removeObserver(this);
  }

  Widget build(BuildContext context) {
    return Consumer<AppThemeNotifier>(
      builder: (BuildContext context, AppThemeNotifier value, Widget child) {
        int themeMode = value.themeMode();
        themeData = AppTheme.getThemeFromThemeMode(themeMode);
        customAppTheme = AppTheme.getCustomAppTheme(themeMode);
        return MaterialApp(
          scaffoldMessengerKey: _scaffoldMessengerKey,
          debugShowCheckedModeBanner: false,
          theme: AppTheme.getThemeFromThemeMode(value.themeMode()),
          home: Scaffold(
              backgroundColor: customAppTheme.bgLayer2,
              key: _scaffoldKey,
              appBar: AppBar(
                backgroundColor: customAppTheme.bgLayer2,
                elevation: 0,
                centerTitle: true,
                title: Text(
                  Translator.translate("welcome_back!"),
                  style: AppTheme.getTextStyle(
                    themeData.textTheme.headline6,
                    color: themeData.colorScheme.onBackground,
                    fontWeight: 600,
                  ),
                ),
                actions: [
                  InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SettingScreen(),
                        ),
                      );
                    },
                    child: Container(
                      margin: Spacing.right(20),
                      child: Icon(
                        MdiIcons.cogOutline,
                        size: MySize.size20,
                        color: themeData.colorScheme.onBackground,
                      ),
                    ),
                  )
                ],
              ),
              floatingActionButton: FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddDeliveryScreen(),
                    ),
                  );
                },
                child: Icon(Icons.add),
              ),
              body: RefreshIndicator(
                onRefresh: _refresh,
                backgroundColor: customAppTheme.bgLayer1,
                color: themeData.colorScheme.primary,
                key: _refreshIndicatorKey,
                child: Column(
                  children: [
                    Container(
                      height: MySize.size3,
                      child: isInProgress
                          ? LinearProgressIndicator(
                              minHeight: MySize.size3,
                            )
                          : Container(
                              height: MySize.size3,
                            ),
                    ),
                    Expanded(
                      child: _buildBody(),
                    ),
                  ],
                ),
              )),
        );
      },
    );
  }

  _buildBody() {
    if (speedometers.length != 0) {
      return GestureDetector(
        onPanUpdate: (details) {
          if (details.delta.dx < 0) {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => SettingScreen()));
          }
        },
        child: ListView(
          padding: Spacing.zero,
          children: [
            Container(
              margin: Spacing.fromLTRB(16, 16, 16, 8),
              child: Text(
                Translator.translate("active").toUpperCase(),
                style: AppTheme.getTextStyle(themeData.textTheme.caption,
                    color: themeData.colorScheme.onBackground,
                    fontWeight: 700,
                    muted: true),
              ),
            ),
            _getActiveSpeedometerView(speedometers),
            Container(
              margin: Spacing.fromLTRB(16, 8, 16, 8),
              child: Text(
                Translator.translate("past").toUpperCase(),
                style: AppTheme.getTextStyle(themeData.textTheme.caption,
                    color: themeData.colorScheme.onBackground,
                    fontWeight: 700,
                    muted: true),
              ),
            ),
            _getPastSpeedometerView(speedometers),
            Container(
              margin: Spacing.fromLTRB(20, 8, 16, 48),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(MySize.size8)),
              ),
              child: SizedBox(
                width: double.infinity,
                child: Text(Translator.translate("just_display_31_data")),
              ),
            ),
          ],
        ),
      );
    } else if (isInProgress) {
      return LoadingScreens.getSpeedometerLoadingScreen(
          context, themeData, customAppTheme);
    } else {
      return ListView(
        padding: Spacing.top(60),
        children: [
          Center(
              child: Text(
            Translator.translate("there_is_no_speedometer_yet"),
            style: AppTheme.getTextStyle(themeData.textTheme.bodyText2,
                color: themeData.colorScheme.onBackground, fontWeight: 600),
          ))
        ],
      );
    }
  }

  _getActiveSpeedometerView(List<Speedometer> speedometers) {
    List<Widget> listViews = [];
    for (int i = 0; i < speedometers.length; i++) {
      if (Speedometer.checkIsActiveSpeedometer(speedometers[i].status)) {
        listViews.add(InkWell(
          onTap: () async {
            //
          },
          child: _singleSpeedometerItem(
              speedometer: speedometers[i], activeSpeedometer: true),
        ));
      }
    }

    if (listViews.length > 0) {
      return Container(
        margin: Spacing.horizontal(16),
        child: Column(
          children: listViews,
        ),
      );
    } else {
      return Container(
        child: Center(
            child: Text(Translator.translate("there_is_no_any_active_data"))),
      );
    }
  }

  _getPastSpeedometerView(List<Speedometer> speedometers) {
    List<Widget> listViews = [];
    for (int i = 0; i < speedometers.length; i++) {
      if (!Speedometer.checkIsActiveSpeedometer(speedometers[i].status)) {
        listViews.add(InkWell(
          onTap: () async {
            //
          },
          child: _singleSpeedometerItem(speedometer: speedometers[i]),
        ));
      }
    }

    if (listViews.length > 0) {
      return Container(
        margin: Spacing.horizontal(16),
        child: Column(
          children: listViews,
        ),
      );
    } else {
      return Container(
        child: Center(
            child: Text(Translator.translate("there_is_no_any_past_data"))),
      );
    }
  }

  _singleSpeedometerItem({Speedometer speedometer, activeSpeedometer = false}) {
    return InkWell(
      onTap: () async {
        if (activeSpeedometer) {
          LocationPermission locationPermission = await Navigator.push(
            context,
            MaterialPageRoute(
              builder: (BuildContext context) => ClosingDeliveryScreen(
                speedometerId: speedometer.id,
              ),
            ),
          );
          _refresh();
          if (locationPermission != null) {
            if (locationPermission == LocationPermission.denied) {
              showMessage(
                  message:
                      Translator.translate("please_give_location_permission"));
            } else if (locationPermission == LocationPermission.deniedForever) {
              Geolocator.openAppSettings();
              showMessage(
                  message: Translator.translate(
                      "please_give_location_permission_in_setting"));
            }
          } else {}
        } else {
          showMessage(
              message: Translator.translate("sorry_data_was_verified!"));
        }
      },
      child: Container(
        padding: Spacing.all(16),
        margin: Spacing.only(top: 4, bottom: 4),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(MySize.size8)),
          color: customAppTheme.bgLayer1,
          border: Border.all(color: customAppTheme.bgLayer3, width: 1.5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        Generator.convertDateTimeToText(
                          speedometer.date,
                          showTime: false,
                        ),
                        style: AppTheme.getTextStyle(
                          themeData.textTheme.subtitle1,
                          fontWeight: 700,
                          letterSpacing: -0.2,
                        ),
                      ),
                      Container(
                        margin: Spacing.only(top: 4),
                        child: Text(
                          speedometer.info,
                          style: AppTheme.getTextStyle(
                            themeData.textTheme.bodyText2,
                            fontWeight: 600,
                            letterSpacing: 0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: Spacing.fromLTRB(12, 8, 12, 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(MySize.size4),
                    ),
                    color: customAppTheme.bgLayer3,
                  ),
                  child: Text(
                    Speedometer.getTextFromSpeedometerStatus(speedometer.status)
                        .toUpperCase(),
                    style: AppTheme.getTextStyle(
                      themeData.textTheme.caption,
                      fontSize: 11,
                      fontWeight: 700,
                      letterSpacing: 0.25,
                    ),
                  ),
                )
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: Spacing.only(top: 8),
                        child: Text(
                          Translator.translate("morning") +
                              ' : ' +
                              Generator.convertDateTimeToText(
                                speedometer.startAt,
                                showDate: false,
                              ),
                          style: AppTheme.getTextStyle(
                            themeData.textTheme.caption,
                            fontWeight: 600,
                            letterSpacing: -0.1,
                            color: themeData.colorScheme.onBackground
                                .withAlpha(160),
                          ),
                        ),
                      ),
                      Container(
                        margin: Spacing.only(top: 8),
                        child: speedometer.startFile == null
                            ? Image.asset(
                                './assets/images/upload-icon.png',
                                color: themeData.colorScheme.primary,
                                width: MySize.getScaledSizeWidth(64),
                                height: MySize.getScaledSizeWidth(64),
                              )
                            : Image.network(
                                speedometer.startFile,
                                width: MySize.getScaledSizeWidth(64),
                                height: MySize.getScaledSizeWidth(64),
                              ),
                      ),
                      Container(
                        margin: Spacing.only(top: 8),
                        child: Text(
                          'Km/ ${speedometer.start}',
                          style: AppTheme.getTextStyle(
                            themeData.textTheme.caption,
                            fontWeight: 600,
                            letterSpacing: -0.1,
                            color: themeData.colorScheme.onBackground
                                .withAlpha(160),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 5,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        margin: Spacing.only(top: 8),
                        child: Text(
                          Translator.translate("afternoon") +
                              ' : ' +
                              Generator.convertDateTimeToText(
                                speedometer.finishAt,
                                showDate: false,
                              ),
                          style: AppTheme.getTextStyle(
                            themeData.textTheme.caption,
                            fontWeight: 600,
                            letterSpacing: -0.1,
                            color: themeData.colorScheme.onBackground
                                .withAlpha(160),
                          ),
                        ),
                      ),
                      Container(
                        margin: Spacing.only(top: 8),
                        child: speedometer.finishFile == null
                            ? Image.asset(
                                './assets/images/upload-icon.png',
                                color: themeData.colorScheme.primary,
                                width: MySize.getScaledSizeWidth(64),
                                height: MySize.getScaledSizeWidth(64),
                              )
                            : Image.network(
                                speedometer.finishFile,
                                width: MySize.getScaledSizeWidth(64),
                                height: MySize.getScaledSizeWidth(64),
                              ),
                      ),
                      Container(
                        margin: Spacing.only(top: 8),
                        child: Text(
                          speedometer.finishFile == null
                              ? 'Upload Lampiran Sore'
                              : 'Km/ ${speedometer.finish}',
                          style: AppTheme.getTextStyle(
                            themeData.textTheme.caption,
                            fontWeight: 600,
                            letterSpacing: -0.1,
                            color: themeData.colorScheme.onBackground
                                .withAlpha(160),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  void showMessage({String message = "Terjadi Kesalahan", Duration duration}) {
    if (duration == null) {
      duration = Duration(seconds: 3);
    }
    _scaffoldMessengerKey.currentState.showSnackBar(
      SnackBar(
        duration: duration,
        content: Text(
          message,
          style: AppTheme.getTextStyle(
            themeData.textTheme.subtitle2,
            letterSpacing: 0.4,
            color: themeData.colorScheme.onPrimary,
          ),
        ),
        backgroundColor: themeData.colorScheme.primary,
        behavior: SnackBarBehavior.fixed,
      ),
    );
  }
}
