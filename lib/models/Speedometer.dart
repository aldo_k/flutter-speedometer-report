import 'package:delivery_boy/services/AppLocalizations.dart';
import 'package:flutter/material.dart';
import 'package:delivery_boy/utils/TextUtils.dart';

// ignore: unused_import
import 'dart:developer';
// ignore: unused_import
import 'DeliveryBoy.dart';

class Speedometer {
  int id, deliveryBoyId, start, finish, status;
  String startFile, finishFile, info;
  DateTime date, startAt, finishAt, createdAt, updatedAt;

  Speedometer(
      this.id,
      this.deliveryBoyId,
      this.date,
      this.start,
      this.startAt,
      this.startFile,
      this.finish,
      this.finishAt,
      this.finishFile,
      this.status,
      this.createdAt,
      this.updatedAt,
      this.info);

  static Speedometer fromJson(Map<String, dynamic> jsonObject) {
    int id = int.parse(jsonObject['id'].toString());
    int deliveryBoyId = int.parse(jsonObject['delivery_boy_id'].toString());
    int start = int.parse(jsonObject['start'].toString());

    int finish = int.tryParse(jsonObject['finish'].toString()) ?? 0;

    String info = 'Belum Closing';
    if (start > 0 && finish > 0) {
      info = 'Total : ${((finish - start) / 10)} Km';
    }

    int status = int.tryParse(jsonObject['status'].toString()) ?? 0;

    String startFile = jsonObject['start_file'] != null
        ? TextUtils.getImageUrl(jsonObject['start_file'].toString())
        : null;
    String finishFile = jsonObject['finish_file'] != null
        ? TextUtils.getImageUrl(jsonObject['finish_file'].toString())
        : null;

    DateTime date = DateTime.parse(jsonObject['date'].toString());

    DateTime startAt = DateTime.parse(jsonObject['start_at'].toString());

    var finishTime = jsonObject['finish_at'] != null
        ? jsonObject['finish_at'].toString()
        : '0000-00-00';

    DateTime finishAt = DateTime.parse(finishTime);
    DateTime createdAt = DateTime.parse(jsonObject['created_at'].toString());
    DateTime updatedAt = DateTime.parse(jsonObject['updated_at'].toString());

    return Speedometer(id, deliveryBoyId, date, start, startAt, startFile,
        finish, finishAt, finishFile, status, createdAt, updatedAt, info);
  }

  static List<Speedometer> getListFromJson(List<dynamic> jsonArray) {
    List<Speedometer> list = [];

    for (int i = 0; i < jsonArray.length; i++) {
      list.add(Speedometer.fromJson(jsonArray[i]));
    }
    return list;
  }

  static String getTextFromSpeedometerStatus(int status) {
    switch (status) {
      case 0:
        return Translator.translate("waiting_confirmation");
      case 1:
        return Translator.translate("data_accepted");
      case 2:
        return Translator.translate("data_rejected");
      default:
        return getTextFromSpeedometerStatus(1);
    }
  }

  static Color getColorFromSpeedometerStatus(int status) {
    switch (status) {
      case 0:
        return Color.fromRGBO(255, 170, 85, 1.0);
      case 1:
        return Color.fromRGBO(90, 149, 154, 1.0);
      case 2:
        return Color.fromRGBO(255, 170, 154, 1.0);
      default:
        return getColorFromSpeedometerStatus(1);
    }
  }

  static bool checkStatusDelivered(int status) {
    return status == 5;
  }

  static bool checkStatusReviewed(int status) {
    return status == 6;
  }

  static bool checkIsActiveSpeedometer(int status) {
    return status == 0 || status == 2;
  }

  static double getDiscountFromCoupon(
      double originalSpeedometerPrice, int offer) {
    return originalSpeedometerPrice * offer / 100;
  }

  static String convertCurrencyToString(double num) {
    return num.toString().replaceAll(RegExp(r"([.]*0)(?!.*\d)"), "");
  }

  @override
  String toString() {
    return 'Speedometer{id $id, deliveryBoyId $deliveryBoyId, date $date, start $start, startAt $startAt, startFile $startFile, finish $finish, finishAt $finishAt, finishFile $finishFile, status $status, createdAt $createdAt, updatedAt $updatedAt}';
  }
}
