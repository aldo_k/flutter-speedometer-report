import 'package:delivery_boy/models/DeliveryBoy.dart';

class AppData {
  final int buildVersion = 140;

  int minBuildVersion;
  DeliveryBoy deliveryBoy;

  AppData(this.minBuildVersion, this.deliveryBoy);

  static AppData fromJson(Map<String, dynamic> jsonObject) {
    int minBuildVersion = int.parse(jsonObject['min_build_version'].toString());

    DeliveryBoy deliveryBoy;
    if (jsonObject['delivery_boy'] != null) {
      deliveryBoy = DeliveryBoy.fromJson(jsonObject['delivery_boy']);
    }

    return AppData(minBuildVersion, deliveryBoy);
  }

  bool isAppUpdated() {
    return buildVersion >= minBuildVersion;
  }

  @override
  String toString() {
    return 'AppData{buildVersion: $buildVersion, minBuildVersion: $minBuildVersion}';
  }
}
