import 'package:delivery_boy/api/api_util.dart';

class MyResponse<T> {
  bool success = false;
  T data;
  List<dynamic> errors = [];
  String errorText = "";
  int responseCode;

  MyResponse(this.responseCode);

  setError(Map<String, dynamic> jsonObject) {
    String error = jsonObject['error'];
    if (error != null) {
      this.errors = [error];
      this.errorText = getFormattedError(this.errors);
      return;
    }
    List<dynamic> errors = jsonObject['errors'];
    if (errors != null) {
      this.errors = errors;
      this.errorText = getFormattedError(errors);
      return;
    }
    this.errorText = "Terjadi Kesalahan";
  }

  static String getFormattedError(List<dynamic> errors) {
    String errorText = "";
    for (int i = 0; i < errors.length; i++) {
      errorText += "- " + errors[i] + (i + 1 < errors.length ? "\n" : "");
    }
    return errorText;
  }

  static makeInternetConnectionError<T>() {
    MyResponse<T> myResponse = MyResponse(ApiUtil.INTERNET_NOT_AVAILABLE_CODE);
    myResponse.errorText = "Harap Hubungkan Ke Internet";
    return myResponse;
  }

  static makeServerProblemError<T>() {
    MyResponse<T> myResponse = MyResponse(ApiUtil.SERVER_ERROR_CODE);
    myResponse.errorText = "Masalah Server! Silahkan Coba Lagi";
    return myResponse;
  }
}
