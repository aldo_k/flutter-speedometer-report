import 'package:delivery_boy/utils/TextUtils.dart';

class DeliveryBoy {
  int id;
  bool isOffline = false;
  String name, email, token, avatarUrl, mobile, motorcycleLicensePlate;

  DeliveryBoy(this.id, this.name, this.email, this.token, this.avatarUrl,
      this.mobile, this.motorcycleLicensePlate, this.isOffline);

  static DeliveryBoy fromJson(Map<String, dynamic> jsonObject) {
    int id = jsonObject['id'];
    String name = jsonObject['name'];
    String email = jsonObject['email'];
    String avatarUrl = jsonObject['avatar_url'];
    String mobile = jsonObject['mobile'];
    String token = jsonObject['token'];
    String motorcycleLicensePlate = jsonObject['motorcycle_license_plate'];
    bool isOffline = TextUtils.parseBool(jsonObject['is_offline']);

    return DeliveryBoy(id, name, email, token, avatarUrl, mobile,
        motorcycleLicensePlate, isOffline);
  }

  @override
  String toString() {
    return 'DeliveryBoy{id: $id, motorcycleLicensePlate: $motorcycleLicensePlate, isOffline: $isOffline, name: $name, email: $email, token: $token, avatarUrl: $avatarUrl, mobile: $mobile}';
  }

  String getAvatarUrl() {
    return TextUtils.getImageUrl(avatarUrl);
  }
}
