import 'dart:convert';
import 'dart:io';
import 'package:delivery_boy/api/api_util.dart';
import 'package:delivery_boy/models/MyResponse.dart';
import 'package:delivery_boy/models/Speedometer.dart';
import 'package:delivery_boy/utils/InternetUtils.dart';
import 'AuthController.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

// ignore: unused_import
import 'dart:developer';

class SpeedometerController {
  /*-----------------   Get all order for currently login user  ----------------------*/
  static Future<MyResponse<List<Speedometer>>> getAllSpeedometer() async {
    //Get Token
    String token = await AuthController.getApiToken();
    String url = ApiUtil.MAIN_API_URL + ApiUtil.SPEEDOMETERS;
    Map<String, String> headers =
        ApiUtil.getHeader(requestType: RequestType.GetWithAuth, token: token);

    //Check Internet
    bool isConnected = await InternetUtils.checkConnection();
    if (!isConnected) {
      return MyResponse.makeInternetConnectionError<List<Speedometer>>();
    }

    try {
      http.Response response = await http.get(url, headers: headers);

      MyResponse<List<Speedometer>> myResponse =
          MyResponse(response.statusCode);
      if (response.statusCode == 200) {
        myResponse.success = true;
        myResponse.data =
            Speedometer.getListFromJson(json.decode(response.body));
      } else {
        Map<String, dynamic> data = json.decode(response.body);
        myResponse.success = false;
        myResponse.setError(data);
      }
      return myResponse;
    } catch (e) {
      return MyResponse.makeServerProblemError<List<Speedometer>>();
    }
  }

  /*-----------------   update order    ----------------------*/
  // Speedometer Status
  // current location in latitude and longitude

  static Future<MyResponse> updateSpeedometer(
      int speedometerId, String digitsKM, File imageFile) async {
    //Get Token
    String token = await AuthController.getApiToken();
    String url = ApiUtil.MAIN_API_URL +
        ApiUtil.SPEEDOMETERS +
        "/" +
        speedometerId.toString();
    Map<String, String> headers =
        ApiUtil.getHeader(requestType: RequestType.PostWithAuth, token: token);

    //Body data
    Map data = {};
    if (digitsKM.isNotEmpty) data['finish'] = digitsKM;

    if (imageFile != null) {
      final bytes = imageFile.readAsBytesSync();
      String img64 = base64Encode(bytes);
      data['finish_file'] = img64;
    }

    //Encode
    String body = json.encode(data);

    //Check Internet
    bool isConnected = await InternetUtils.checkConnection();
    if (!isConnected) {
      return MyResponse.makeInternetConnectionError();
    }

    try {
      http.Response response =
          await http.put(url, headers: headers, body: body);

      log('${response.body}');

      MyResponse myResponse = MyResponse(response.statusCode);

      if (response.statusCode == 200) {
        myResponse.success = true;
      } else {
        Map<String, dynamic> data = json.decode(response.body);
        myResponse.success = false;
        myResponse.setError(data);
      }
      return myResponse;
    } catch (e) {
      return MyResponse.makeServerProblemError();
    }
  }

  /*-----------------   Get single order from order id    ----------------------*/
  static Future<MyResponse> addSpeedometer(
      String digitsKM, File imageFile) async {
    //Get Token
    String token = await AuthController.getApiToken();
    String url = ApiUtil.MAIN_API_URL + ApiUtil.SPEEDOMETERS;

    Map data = {};

    if (digitsKM.isNotEmpty) data['start'] = digitsKM;

    if (imageFile != null) {
      final bytes = imageFile.readAsBytesSync();
      String img64 = base64Encode(bytes);
      data['start_file'] = img64;
    }

    //Encode
    String body = json.encode(data);

    //Check Internet
    bool isConnected = await InternetUtils.checkConnection();
    if (!isConnected) {
      return MyResponse.makeInternetConnectionError();
    }

    try {
      Response response = await http.post(
        url,
        headers: ApiUtil.getHeader(
            requestType: RequestType.PostWithAuth, token: token),
        body: body,
      );

      log('${response.body}');

      MyResponse myResponse = MyResponse(response.statusCode);
      if (response.statusCode == 200) {
        myResponse.success = true;
      } else {
        Map<String, dynamic> data = json.decode(response.body);
        myResponse.success = false;
        myResponse.setError(data);
      }

      return myResponse;
    } catch (e) {
      return MyResponse.makeServerProblemError();
    }
  }

  /*-----------------   Get single order from order id    ----------------------*/

  static Future<MyResponse<Speedometer>> getSingleSpeedometer(int id) async {
    //Get Token
    String token = await AuthController.getApiToken();
    String url =
        ApiUtil.MAIN_API_URL + ApiUtil.SPEEDOMETERS + "/" + id.toString();
    Map<String, String> headers =
        ApiUtil.getHeader(requestType: RequestType.GetWithAuth, token: token);

    //Check Internet
    bool isConnected = await InternetUtils.checkConnection();
    if (!isConnected) {
      return MyResponse.makeInternetConnectionError<Speedometer>();
    }

    try {
      http.Response response = await http.get(url, headers: headers);
      MyResponse<Speedometer> myResponse = MyResponse(response.statusCode);
      if (response.statusCode == 200) {
        myResponse.success = true;
        myResponse.data = Speedometer.fromJson(json.decode(response.body));
      } else {
        Map<String, dynamic> data = json.decode(response.body);
        myResponse.success = false;
        myResponse.setError(data);
      }
      return myResponse;
    } catch (e) {
      return MyResponse.makeServerProblemError<Speedometer>();
    }
  }
}
